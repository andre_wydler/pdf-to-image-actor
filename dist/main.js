"use strict";
// This is  main Node.js source code file of your actor.
// It is referenced from  "scripts" section of  package.json file,
// so that it can be started by running "npm start".
Object.defineProperty(exports, "__esModule", { value: true });
// Import Apify SDK. For more information, see https://sdk.apify.com/
const FS = require("fs");
const apify_1 = require("apify");
const GM = require("gm");
const gm = GM.subClass({ imageMagick: true });
apify_1.default.main(async () => {
    // Get input of  actor.
    // If you'd like to have your input checked and have Apify display
    // a user interface for it, add INPUT_SCHEMA.json file to your actor.
    // For more information, see https://docs.apify.com/actors/development/input-schema
    const input = await apify_1.default.getInput();
    console.log('Input:');
    console.dir(input);
    // First we create  request queue instance.
    const requestQueue = await apify_1.default.openRequestQueue();
    // And n we add a request to it.
    await requestQueue.addRequest({ url: 'https://cnn.com' });
    // Do something useful here...
    const handlePageFunction = async ({ request, $ }) => {
        const title = $('title').text();
        console.log(`Title of "${request.url}" is: ${title}.`);
    };
    // Set up  crawler, passing a single options object as an argument.
    const crawler = new apify_1.default.CheerioCrawler({
        requestQueue,
        handlePageFunction,
    });
    await crawler.run();
    function streamToBuffer(source, destination) {
        return new Promise((resolve, reject) => {
            gm(source)
                .density(600, 600)
                .resize(1440, 2550)
                .quality(100)
                .flatten()
                .strip()
                .write(destination, function (err) {
                if (err) {
                    reject();
                }
                resolve(undefined);
            });
        });
    }
    try {
        await streamToBuffer('__artifacts__/nyt.pdf[0]', '__artifacts__/output/nyt.jpg');
        // await streamToBuffer(
        //   "__artifacts__/nzz.pdf[0]",
        //   "__artifacts__/output/nzz.jpg"
        // );
    }
    catch (error) {
        console.error(error);
    }
    // Save output
    const output = {
        receivedInput: input,
        message: 'Hello sir!',
    };
    console.log('Output:');
    console.dir(output);
    const buffer = FS.readFileSync('__artifacts__/output/nyt.jpg');
    console.log('Image:');
    console.dir(buffer);
    await apify_1.default.setValue('IMAGE', buffer, { contentType: 'image/jpeg' });
    await apify_1.default.setValue('OUTPUT', output);
});
